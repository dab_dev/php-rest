<?php
require_once "PeopleDB.php";
class PeopleAPI {
	public function API(){
		header('Content-Type: application/JSON');
		$method = $_SERVER['REQUEST_METHOD'];
		switch ($method) {
			case 'GET':
				$this->getPeoples();
				break;
			case 'POST':
				echo 'POST';
				break;
			case 'PUT':
				echo 'PUT';
				break;
			case 'DELETE':
				echo 'DELETE';
				break;
			default:
				echo 'METODO NO SOPORTADO';
				break;
		}
	}
	
	function getPeoples(){
	    if($_GET['action']=='peoples'){
	        $db = new PeopleDB();
	        if(isset($_GET['id'])){
	            $response = $db->getPeople($_GET['id']);
	            echo json_encode($response,JSON_PRETTY_PRINT);
	        }else{ 
	            $response = $db->getPeoples();
	            echo json_encode($response,JSON_PRETTY_PRINT);
	        }
	    }else{
		        $this->response(400);
		}
	}
}

					
					
<?php
class PeopleDB {

	protected $mysqli;
	const LOCALHOST = '127.0.0.1';
	const USER = 'root';
	const PASSWORD = '';
	const DATABASE = 'dbtest';

	public function __construct() {
		try{
			$this->mysqli = new mysqli(self::LOCALHOST, self::USER, self::PASSWORD, self::DATABASE);
		}catch (mysqli_sql_exception $e){
			http_response_code(500);
			exit;
		}
	}

	public function getPeople($id=0){
		$stmt = $this->mysqli->prepare("SELECT * FROM people WHERE id=? ; ");
		$stmt->bind_param('s', $id);
		$stmt->execute();
		$result = $stmt->get_result();
		$peoples = $result->fetch_all(MYSQLI_ASSOC);
		$stmt->close();
		return $peoples;
	}

	public function getPeoples(){
		$result = $this->mysqli->query('SELECT * FROM people');
		$peoples = $result->fetch_all(MYSQLI_ASSOC);
		$result->close();
		return $peoples;
	}
	
	function response($code=200, $status="", $message="") {
		http_response_code($code);
		if( !empty($status) && !empty($message) ){
			$response = array("status" => $status ,"message"=>$message);
			echo json_encode($response,JSON_PRETTY_PRINT);
		}
	}
}